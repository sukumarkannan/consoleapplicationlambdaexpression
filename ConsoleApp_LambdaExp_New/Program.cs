﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp_LambdaExp_New
{
    class Program
    {
        //public delegate bool MyLambdaExpression(int a, int b);

        public static void MyFunction()
        {
            Console.WriteLine("First Hello");
			Console.WriteLine("After Git Commiting");
        }

        delegate void MyDel();
        static void Main(string[] args)
        {
            //MyLambdaExpression mylamb = (a, b) => a < b;
            //Console.WriteLine(mylamb(10, 20));
            //Console.ReadLine();

			// Working Code
            MyDel d = () =>
            {
                Program.MyFunction();
                Console.WriteLine("This is Lambda Expression");
            };
            d.Invoke();
            Console.ReadLine();
        }
    }
}
